import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Home from "./HomePage";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/:locale">
          <Home />
        </Route>
        {/* Default locale */}
        <Redirect from="/" to="/en" />
      </Switch>
    </Router>
  );
}

export default App;
