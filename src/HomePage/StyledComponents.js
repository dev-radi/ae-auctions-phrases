import styled from "styled-components";

export const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const PhrasesBox = styled.div`
  width: 90%;
`;

export const PhrasesListGrid = styled.div`
  display: grid;
  grid-template-columns: 20% 1fr;
  grid-column-gap: 20px;
`;

export const StyledNav = styled.nav`
  display: flex;
  flex-wrap: wrap;
  background-color: #ffa5a5;
  padding: 10px 0;
  list-style-type: none;
  margin: 0;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  width: 100%;

  & > li,
  a {
    background-color: #fcdada;
    padding: 10px;
    margin: 10px;
  }
`;

export const StyledButton = styled.button`
  background-color: lightblue;
  margin: 10px;
  ${props => props.isActive && `
    background-color: #ec524b;
  `}
`;
