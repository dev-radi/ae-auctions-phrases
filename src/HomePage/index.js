import React, { memo, useCallback, useEffect, useRef, useState } from "react";
import { useParams, NavLink } from "react-router-dom";
import {
  PhrasesListGrid,
  Wrapper,
  PhrasesBox,
  StyledNav,
  StyledButton,
} from "./StyledComponents";
import PaginationBar from "../Components/PaginationBar";
import { usePageNumber } from "../hooks";

const PHRASES_PER_PAGE = 3;
const TOTAL_PAGES = 3;

const POLLING_RATE = 30000;

const HomePage = memo(() => {
  const [phrases, setPhrases] = useState();
  const pollingIntervalIdRef = useRef();
  const [isPollingEnabled, setPollingEnabled] = useState();
  const { locale } = useParams();
  const page = usePageNumber();

  const fetchPhrases = useCallback(async () => {
    const apiResponse = await fetch(
      "https://api.emiratesauction.com/v2/phrases?source=mweb"
    );

    const result = await apiResponse.json();

    setPhrases(result.Phrases);
  }, [setPhrases]);

  // Activate/de-activate continuous fetching. 
  const togglePolling = useCallback(() => {
    setPollingEnabled(!isPollingEnabled);
  }, [isPollingEnabled, setPollingEnabled]);

  // Loads "phrases" on mounting, only once.
  useEffect(() => {
    fetchPhrases();
  }, [fetchPhrases]);


  // Handles polling activation.
  useEffect(() => {
    if (pollingIntervalIdRef.current) {
      clearInterval(pollingIntervalIdRef.current);
    }

    if (isPollingEnabled) {
      pollingIntervalIdRef.current = setInterval(
        () => fetchPhrases(),
        POLLING_RATE
      );
    }
  }, [isPollingEnabled, fetchPhrases]);

  if (!phrases) {
    return <h2>loading ...</h2>;
  }

  // Pagination logic.
  const lastPhraseInPageIndex = page * PHRASES_PER_PAGE;
  const firstPhraseInPageIndex = lastPhraseInPageIndex - PHRASES_PER_PAGE;
  const totalPhrasesForPages = phrases.slice(0, TOTAL_PAGES * PHRASES_PER_PAGE);

  const currentPhrasesInPage = totalPhrasesForPages.slice(
    firstPhraseInPageIndex,
    lastPhraseInPageIndex
  );

  return (
    <Wrapper>
      <StyledNav>
        <NavLink to={`/ar?page=${page}`}>AR</NavLink>
        <NavLink to={`/en?page=${page}`}>EN</NavLink>
      </StyledNav>
      <StyledButton onClick={togglePolling} isActive={isPollingEnabled}>
        Polling {isPollingEnabled ? "Enabled, Fetching Data every 30 Seconds" : "Disabled"}
      </StyledButton>
      <h4>Phrases - {locale}</h4>

      <PhrasesBox>
        <PhrasesListGrid>
          <h6>Key</h6>
          <h6>Translation</h6>
        </PhrasesListGrid>

        {currentPhrasesInPage.map((phrase) => {
          return (
            <PhrasesListGrid key={phrase.key}>
              <h6>{phrase.key}</h6>
              <h6>{phrase[locale]}</h6>
            </PhrasesListGrid>
          );
        })}
      </PhrasesBox>

      <PaginationBar
        locale={locale}
        currentPageNumber={page}
        totalPages={TOTAL_PAGES}
        phrasesPerPage={PHRASES_PER_PAGE}
      />
    </Wrapper>
  );
});

export default HomePage;
