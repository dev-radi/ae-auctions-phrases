import useQuery from "./useQuery";
import usePageNumber from './usePageNumber'

export { useQuery, usePageNumber };
