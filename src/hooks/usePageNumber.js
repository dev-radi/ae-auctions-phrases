import useQuery from "./useQuery";

const usePageNumberQuery = () => {
  const query = useQuery();

  const page = query.get("page") || 1;

  return Number(page);
};

export default usePageNumberQuery;
