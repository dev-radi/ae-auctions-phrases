import React, { memo } from "react";
import { Link, useParams } from "react-router-dom";
import styled from "styled-components";
import { usePageNumber } from "../hooks";

const range = (size, startAt = 0) => {
  return [...Array(size).keys()].map((i) => i + startAt);
};

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #f2dcbb;
`;

const StyledLink = styled(({ isActivePage, ...otherProps }) => (
  <Link {...otherProps} />
))`
  margin-right: 10px;
  padding: 10px;
  ${(props) =>
    props.isActivePage &&
    `
    background-color: #fd3a69;
  `}
`;

const PaginationBar = memo(({ totalPages }) => {
  const page = usePageNumber();
  const { locale } = useParams();

  const pagesArray = range(totalPages, 1);

  return (
    <Wrapper>
      {pagesArray.map((number) => (
        <StyledLink
          to={`/${locale}?page=${number}`}
          key={number}
          isActivePage={page === number}
        >
          {number}
        </StyledLink>
      ))}
    </Wrapper>
  );
});

export default PaginationBar;
